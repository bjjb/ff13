# Quick and dirty AJAX
xhr = (method, url, data, cb) ->
  r = new XMLHttpRequest()
  r.open(method, url, true)
  r.addEventListener 'readystatechange', -> cb(r) if r.readyState is 4
  r.send(data)

xhr.get = (url, cb) -> xhr('GET', url, undefined, cb)

# Quick and dirty jQuery-like element lookup
$ = (x) -> document.querySelector(x)
$$ = (x) -> document.querySelectorAll(x)
$name = (x) -> $("[name='#{x}']")

# This is the database we're using - populated after init
WEAPONS = null
ACCESSORIES = null
ORGANIC = null
MECHANICAL = null
READY = false

xhr.get '/accessories.json', (xhr) -> ACCESSORIES = JSON.parse(xhr.responseText)
xhr.get '/organic.json', (xhr) -> ORGANIC = JSON.parse(xhr.responseText)
xhr.get '/mechanical.json', (xhr) -> MECHANICAL = JSON.parse(xhr.responseText)
# we get weapons after init, because we use it to fill in the DOM

# Called to change the character - repopulates the available weapons
setCharacter = (name) ->
  name = @value if name?.target?
  $('img#character')?.src = "/#{name}.png"
  [weapons] = [w for w in WEAPONS when w.character is name]
  [options] = ["<option value=\"#{w.name}\">#{w.name}</option>" for w in weapons]
  $name('weapon').innerHTML = options.join("\n")
  $name('weapon').value = weapons[0].name

# Called to change the weapon - resets the form
# TODO - save the experience per weapon in local storage
setWeapon = (name) ->
  name = @value if name?.target?
  weapon = findWeapon(name)
  document.querySelector('img#weapon')?.src = "/#{name}.png"
  $name('experience').max = "#{weapon.exp}"
  showRequirements()

showRequirements = ->
  showMultipliers()
  showUpgrades()

init = ->
  $name('character').addEventListener('change', setCharacter, false)
  $name('weapon').addEventListener('change', setWeapon, false)
  $name('experience').addEventListener('change', showRequirements, false)
  $name('multiplier').addEventListener('change', showRequirements, false)
  xhr.get '/weapons.json', (xhr) ->
    WEAPONS = JSON.parse(xhr.responseText)
    $name('character').value = WEAPONS[0].character
    $name('weapon').value = WEAPONS[0].name
    setCharacter WEAPONS[0].character
    setWeapon WEAPONS[0].name
document.addEventListener 'DOMContentLoaded', init

currentMultiplier = -> Number($name('multiplier').value)

findWeapon = (name) ->
  name ?= $name('weapon').value
  return w for w in WEAPONS when w.name is name
findComponent = (name) ->
  return c for c in ORGANIC if c.name is name
  return c  for c in MECHANICAL when c.name is name

requiredMultiplier = ->
  current = switch currentMultiplier()
    when 1 then 0
    when 1.25 then 51
    when 1.5 then 101
    when 1.75 then 201
    when 2 then 251
    when 3 then 501
  501 - current

showMultipliers = ->
  return unless ORGANIC
  required = requiredMultiplier()
  $('#mult-required').innerHTML = required
  return $('#multiplier').hidden = true if required is 0
  # Only showing components with a price (which can be bought)
  components = for c in ORGANIC.filter((x) -> x.buy and x.mult and x.mult > 0)
    qty = Math.ceil(required / c.mult)
    total = qty * Number(c.buy)
    { name: c.name, qty: qty, price: c.buy, exp: c.exp, total: total, availability: c.availability }
  $('#multiplier tbody').innerHTML = renderComponents(components)
  $('#multiplier').hidden = false

showUpgrades = ->
  return unless MECHANICAL
  required = findWeapon().exp - Number($name('experience').value)
  $('#exp-required').innerHTML = required
  return $('#upgrade').hidden = true if required is 0
  # Only showing components with a price (which can be bought)
  components = for c in MECHANICAL.filter((x) -> x.buy and Number(x.exp))
    qty = Math.ceil(required / (c.exp * currentMultiplier()))
    total = qty * Number(c.buy)
    { name: c.name, qty: qty, price: c.buy, exp: c.exp, total: total, availability: c.availability }
  $('#upgrade tbody').innerHTML = renderComponents(components)
  $('#upgrade').hidden = false

renderComponent = (c) ->
  "<tr>
    <td>#{c.qty}</td>
    <td>#{c.name}</td>
    <td>#{c.total}</td>
    <td>#{c.availability}</td>
  </tr>"

renderComponents = (components, limit = 5) ->
  components = components.sort((a, b) -> a.total - b.total)[0...limit]
  (renderComponent(c) for c in components).join("\n")
